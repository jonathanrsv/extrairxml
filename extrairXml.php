<?php
header('Content-Type: text/html; charset=utf-8');
include "functions.php";
include "config.php";

$sql = "SELECT * FROM lojas";
$query = $mysqli->query($sql);
while ($row = mysqli_fetch_array($query)) { // Busca a lista de xml gravada no banco
    $xml = $row['xml'];
    $processados = 0;
    $existentes  = 0;
    $produtos = new XMLReader();
    $produtos->open($xml);
    while($produtos->read()){
        switch($produtos->nodeType){
            case (XMLReader::ELEMENT):
                if($produtos->localName == 'item'){
                    $node = $produtos->expand();
                    $dom  = new DOMDocument();
                    $n    = $dom->importNode($node, true);
                    $dom->appendChild($n);
                    $simple_xml = simplexml_import_dom($n);
                    $dc   = $simple_xml ->children('g', TRUE); // namespaces do xml
                    $id         = $dc->id;
                    $titulo     = $simple_xml->title;
                    $descricao  = strip_tags($simple_xml->description);
                    $preco      = moeda($dc->price);
                    $img        = salve_img($dc->image_link,limpaString($titulo),$row['id_loja']); // salva a imagem na pasta de destino definida
                    $slug       = limpaString($simple_xml->title);
                    $url        = $simple_xml->link;
					if($dc->availability == "in stock"){$status = 1;} else {$status = 0;}
                    $id_xml = "SELECT id_xml FROM produtos WHERE id_xml = '".$id."'";
                    $busca = $mysqli->query($id_xml);
                    if($busca->num_rows == 0) { // verifica se o produtos já existe, se sim, efetua o update (Fazer)
                        $insert = " INSERT INTO `produtos` (`id_loja`, `id_xml`, `slug`,`titulo`, `descricao`, `img`, `preco`, url, status)
                                    VALUES
                                    ('".$row['id_loja']."', '".$id."', '".$slug."', '".$titulo."', '".$descricao."', '".$img."', '".$preco."', '".$url."', '".$status."')";
                        $queryinsert  = $mysqli->query($insert);
                        $processados++;
                    } else {
                        $existentes++;

                }
            }
        }
    }
}
echo "total de produtos porcessados: ".$processados."<br>";
echo "Total de produtos existentes:".$existentes;
?>
